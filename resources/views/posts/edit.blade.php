@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>


			
			<form   action="{{ route('admin.posts.edit', ['id' => $post->id ]); }}" method="post">
                <div class="card-body">
                    <h1>Posts</h1>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />

					<div class="form-group">
						<label>title</label>
						<input class="form-control" type="text" value="{{ $post->title }}" name="title">
					</div>
						
					<div class="form-group">	
							<label>description</label>
							<textarea class="form-control" type="text" name="description">{{ $post->description }}</textarea>
					</div>
					
					<div class="form-group">	
						<label>contact_number</label>
						<input class="form-control" type="text" value="{{ $post->contact_number }}" name="contact_number">
					</div>
                </div>
				
				<div class="form-group">	
						 
						<input class="form-control" type="submit" name="submit">
					</div>
			</form>	
				
				
            </div>
        </div>
    </div>
</div>
@endsection
