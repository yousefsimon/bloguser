@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    <h1>Posts</h1>
                    <a href="{{ url('/admin/post/create') }}">create post</a>
                    <table class="table table-bordered">
                        
                        <thead>
                            <tr>
                                <th> id </th>
                                <th> title </th>
                                <th> action </th>   
                            </tr>    
                        </thead>
                        <tbody>
                          @foreach($posts as $post)
                                 <tr>
                                        <td> {{ $post->id }} </td>
                                        <td> {{ $post->title }} </td>
                                        <td>
											 
											<a href="{{ route('admin.posts.edit', ['id' => $post->id ]); }}">edit</a>
											<br>
										<a  onclick="deleteSales('{{ route('admin.posts.delete', ['id' => $post->id]) }}')" href="javascript:void(0)">delete</a>
										</td>   
                                    </tr> 
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
