<!-- resources/views/emails/send_email.blade.php -->

<!DOCTYPE html>
<html>
<head>
    <title>Email</title>
</head>
<body>
    <h1>Posts</h1>
              <table class="table table-bordered">
                        
                        <thead>
                            <tr>
                                <th> id </th>
                                <th> title </th> 
                            </tr>    
                        </thead>
                        <tbody>
                          @foreach($posts as $post)
                                 <tr>
                                        <td> {{ $post->id }} </td>
                                        <td> {{ $post->title }} </td>
                                   
                                    </tr> 
                            @endforeach
                        </tbody>
                    </table>
					
			 <h1>Users</h1>
              <table class="table table-bordered">
                        
                        <thead>
                            <tr>
                                <th> id </th>
                                <th> title </th>  
                            </tr>    
                        </thead>
                        <tbody>
                          @foreach($users as $user)
                                 <tr>
                                        <td> {{ $user->id }} </td>
                                        <td> {{ $user->name }} </td>
                                    
                            @endforeach
                        </tbody>
                    </table>		
					
</body>
</html>