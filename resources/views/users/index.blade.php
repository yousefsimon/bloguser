@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    <h1>Users</h1>
                    <a href="{{ url('/admin/user/create') }}">create user</a>
                    <table class="table table-bordered">
                        
                        <thead>
                            <tr>
                                <th> id </th>
                                <th> title </th>
                                <th> action </th>   
                            </tr>    
                        </thead>
                        <tbody>
                          @foreach($users as $user)
                                 <tr>
                                        <td> {{ $user->id }} </td>
                                        <td> {{ $user->name }} </td>
                                        <td>
											 
											<a href="{{ route('admin.users.edit', ['id' => $user->id ]); }}">edit</a>
											<br>
											<a  onclick="deleteSales('{{ route('admin.users.delete', ['id' => $user->id]) }}')" href="javascript:void(0)">delete</a>
										</td>   
                                    </tr> 
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
