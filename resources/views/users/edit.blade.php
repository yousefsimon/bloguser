@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

						@if ($errors->any())
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
			
			<form   action="{{ route('admin.users.edit', ['id' => $user->id ]); }}" method="post">
                <div class="card-body">
                    <h1>users</h1>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />

					<div class="form-group">
						<label>name</label>
						<input class="form-control" type="text" value="{{ $user->name }}" name="name">
					</div>
						
					<div class="form-group">	
							<label>mobile_number</label>
								<input class="form-control" type="text" value="{{ $user->mobile_number }}" name="mobile_number">
					</div>
					
					 
					
					<div class="form-group">	
						<label>email</label>
						<input class="form-control"  value="{{ $user->email }}" type="text" name="email">
					</div>
					   
					<div class="form-group">	
						<label>password</label>
						  <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
					</div>
					
					
						<div class="form-group">	
						<label>password_confirmation</label>
						 <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
					</div>
                </div>
				
				<div class="form-group">	
						 
						<input class="form-control" type="submit" name="submit">
					</div>
			</form>	
				
				
            </div>
        </div>
    </div>
</div>
@endsection
