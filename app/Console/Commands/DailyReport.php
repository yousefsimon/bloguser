<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mail\DailyEmail;


use App\Models\User;
use App\Models\Post;


class DailyReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:daily-report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
			$users=User::all();
			$posts=Post::all();

			Mail::to('recipient@example.com')->send(new DailyEmail($users,$posts));
    }
}
