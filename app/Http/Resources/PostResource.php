<?php
namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
  
			$arr= [
				  'id' => $this->id,
				  'title' => $this->title,
				  'description' => $this->description,
				  'contact_number' => $this->contact_number,
			];
			
			if($this->user!=null){
				$arr['author']=$this->user->name;
			}
			return $arr;
    }
}