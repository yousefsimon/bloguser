<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;

class PostController extends Controller
{
    public function index() {
        
        
        $posts = Post::paginate(15);
          return view('posts.index', [  'posts' => $posts   ]);
    }
	
	
    public function create() {
       // $posts = Post::paginate(15);
          return view('posts.create');
    }
	
     public function store() {
         
      $data = request()->validate([
             'title'=>'required',
             'description'=>'required',
             'contact_number'=>'required',
         ]);
      
          Post::create($data);
          return redirect('/admin/posts');
    }
	
    
    
    
    	
    public function edit($id) {
		
        $post=Post::find($id); 
		return view('posts.edit', compact('post'));
    }
    
	
	 public function update($id) {
		    $data = request()->validate([
             'title'=>'required',
             'description'=>'required',
             'contact_number'=>'required',
         ]);
        $post=Post::find($id);
		$post->update( $data);
		 return redirect('/admin/posts');
    }
	
	
	 public function delete_post($id) {
		
        $post=Post::find($id);
		 
		$post->delete(); //returns true/false

		  return redirect('/admin/posts');
    }
   
	
}
