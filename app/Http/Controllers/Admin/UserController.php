<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\User;

class UserController extends Controller
{
    public function index() {
        
        
        $users = User::paginate(15);
          return view('users.index', [  'users' => $users   ]);
    }
	
	
    public function create() {
       // $posts = Post::paginate(15);
          return view('users.create');
    }
	
     public function store() {
         
  	 $data = request()->validate([
             'name'=>'required',
             'mobile_number'=>'required',
             'email'=>'required|email',
			 'password' => 'required|min:8|confirmed',
         ]);
      
          User::create($data);
          return redirect('/admin/users');
    }
	
    
    
    
    	
    public function edit($id) {
		
        $user=User::find($id); 
		return view('users.edit', compact('user'));
    }
    
	
	 public function update($id) {
	 $data = request()->validate([
             'name'=>'required',
             'mobile_number'=>'required',
             'email'=>'required|email',
			 'password' => 'required|min:8|confirmed',
         ]);
        $user=User::find($id);
		 
		$user->update( $data);
		 return redirect('/admin/users');
    }
	
	
	 public function delete_user($id) {
		
        $user=User::find($id);
		 
		$user->delete(); //returns true/false

		  return redirect('/admin/users');
    }
   
	
}
