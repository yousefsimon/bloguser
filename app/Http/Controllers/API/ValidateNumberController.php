<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Services\Twilio; 

class ValidateNumberController extends Controller {

    public function __construct(private Twilio $twillo) {
        
    }

     

    public function verifyNumber(Request $request) {
        $mobileNumber = $request->get('mobile_number');

        $this->twillo->verifyNumber($mobileNumber);

        return response()->json([
                    'message' => 'done u have enter the number',
                    'code' => 200
        ]);
    }

    public function verifyCode(Request $request) {
        $code = $request->get('code');

        $this->twillo->verifyCode($code);

        return response()->json([
                    'message' => 'u mobile is verfied',
                    'code' => 200
        ]);
    }
	
	 

}
