<?php


namespace App\Http\Controllers\Api;


use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Validator;

trait Response
{
    public function json(?LengthAwarePaginator $collection = null, $data = [], $status = true, $message = null, $pagination = true, $headerCode = 200)
    {
        if ($message === null) $message = trans('app.success');

        $res = [
            'status' => $status,
            'data' => $data,
            'message' => $message
        ];

        if ($pagination && $collection instanceof LengthAwarePaginator)
            $res['pagination'] = $this->pagination($collection);

        return response()->json($res, $headerCode);
    }

    public function pagination(LengthAwarePaginator $paginator)
    {
        return [
            'total' => $paginator->total(),
            'per_page' => $paginator->perPage(),
            'current_page' => $paginator->currentPage(),
            'last_page' => $paginator->lastPage(),
			 'prev_page_url' => $paginator->previousPageUrl(),
			 'next_page_url' => $paginator->nextPageUrl()
        ];
    }

    public function validData($data, $roles)
    {
        $validatedData = Validator::make($data, $roles);
 
        if ($validatedData->errors()->count()) {
					return response()->json(['message' =>trans('app.info_not_found') ,'errors'=> $validatedData->errors()->toArray(),'status' => false], 200);
					
					//   return $this->json(null, $validatedData->errors()->toArray(), false, trans('responses.form_validation_failed'), null, 400);
			
        }
        return false;
    }
}
