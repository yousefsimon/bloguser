<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Services\Twilio;
use App\Models\Post;
 
use App\Http\Resources\PostResource;
use App\Http\Resources\UserResource;

class AuthController extends Controller {

    public function __construct(private Twilio $twillo) {
        
    }

    public function register(Request $request) {
        $data = $request->all();

        // return response([ 'user' => $data['form']]);
        // dd($data);

        $validator = Validator::make($data, [
                    'name' => 'required',
                     
                    'mobile_number' => 'required',
                    'email' => 'email|required|unique:users',
                    'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                        'status' => $validator->errors()
            ]);
        }


        $data['password'] = bcrypt($data['password']);
        //   $data['password'] = bcrypt($data['password']);

        $user = User::create($data);

        $accessToken = $user->createToken('authToken')->accessToken;

        return response(['user' => $user, 'access_token' => $accessToken]);
    }

    public function login(Request $request) {
        $data = $request->all();

        $loginData = $request->validate([
            'mobile_number' => 'required',
            'password' => 'required'
        ]);

        if (!auth()->attempt($loginData)) {
            return response(['status' => false, 'message' => 'Invalid Credentials']);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;

        return response(['user' => auth()->user(), 'access_token' => $accessToken, 'status' => true]);
    }

    public function profile(Request $request) {
          $customer = auth('api')->user();

           
          return response()->json([
          'profile' =>$customer, 
          ]);  
    }
 

}
