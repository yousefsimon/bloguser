<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
 
use App\Http\Resources\PostResource;
use App\Http\Resources\UserResource;
use App\Models\Post;
 
class PostController extends Controller {

 
     


    public function users(Request $request) {
       $users= User::paginate(2);

        return response()->json([
                    'users' => UserResource::collection($users)->response()->getData(true), 
        ]);
    }
	
	 public function posts(Request $request) {
         $posts= Post::orderBy('created_at','desc')->paginate(2);

        return response()->json([
                    'posts' => PostResource::collection($posts)->response()->getData(true), 
        ]);
    }
	
	
	
	 public function create_post(Request $request) {
		 
         $validatedData = $request->validate([
				'title' => 'required|max:255',
				'description' => 'required|max:2048',
				'contact_number' => 'required',
			]);
			
			$validatedData['user_id']=  auth()->user()->id;
			 
			
		$post= Post::create($validatedData);
        return response()->json([
                    'posts' =>new PostResource($post), 
        ]);
    }
	
	 

}
