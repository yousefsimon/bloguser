<?php

namespace App\Services;


use Twilio\Rest\Client;

class Twilio{
    
    private $client;

    public function  __construct() { 
            // Twilio credentials
            $accountSid = 'YOUR_TWILIO_ACCOUNT_SID';
            $authToken = 'YOUR_TWILIO_AUTH_TOKEN';

            // Initialize Twilio client
            $this->client = new Client($accountSid, $authToken); 
           // return $client;
    }    
    
    public function verifyNumber($number) {
        $verification = $this->client->verify->v2->services('YOUR_TWILIO_VERIFY_SERVICE_SID')
                                  ->verifications
                                  ->create($number, 'sms');

// Capture the verification SID for later use
            $verificationSid = $verification->sid;
             return    $verificationSid;
    }
    
    
    public function verifyCode($code) {
        $verificationCheck = $client->verify->v2->services('YOUR_TWILIO_VERIFY_SERVICE_SID')
                                        ->verificationChecks
                                        ->create($verificationSid, ['code' => '123456']);

                // Check the verification status
                if ($verificationCheck->status === 'approved') {
                    return true;
                } else {
                    // Verification failed
                    return true;
                }
    }
    

}