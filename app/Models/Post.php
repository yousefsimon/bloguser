<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\User;

class Post extends Model
{
    use HasFactory;
	
	protected $table = 'posts';

    protected $fillable = [
        'title',
        'description', 
        'contact_number', 
        'user_id', 
    
    ];
    
    
    protected $dates = [
        'created_at',
        'updated_at',
    
    ];
	
	 
	 public function user(){
		return $this->belongsTo(User::class, 'user_id');
	}
	
}
