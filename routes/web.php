<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\PostController;
use App\Http\Controllers\Admin\UserController;

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect('/admin/dashboard');
});


Route::get('/admin/register',[DashboardController::class,'adminsregister'])->name('adminsregister');

Route::get('/admin/login',[DashboardController::class,'adminsdashboard'])->name('adminslogin');
Route::get('/admin/login',[DashboardController::class,'adminsdashboard'])->name('adminslogin');

Route::get('/admin/dashboard',[DashboardController::class,'adminsdashboard'])->name('adminsdashboard')->middleware('auth:admin');

Auth::routes();




Route::get('/admin',[LoginController::class,'showAdminLoginForm'])->name('admin.login-view');
Route::post('/admin',[LoginController::class,'adminLogin'])->name('admin.login');

Route::get('/admin/register',[RegisterController::class,'showAdminRegisterForm'])->name('admin.register-view');
Route::post('/admin/register',[RegisterController::class,'createAdmin'])->name('admin.register');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth:admin']], function () {
	
		Route::get('/admin/posts',[PostController::class,'index'])->name('admin.posts');

		Route::get('/admin/post/create',[PostController::class,'create'])->name('admin.posts.create');

		Route::post('/admin/post/create',[PostController::class,'store'])->name('admin.posts.store');

		Route::get('/admin/post/{id}',[PostController::class,'edit'])->name('admin.posts.edit');

		Route::post('/admin/post/{id}',[PostController::class,'update'])->name('admin.posts.update');

		Route::post('/admin/post/{id}/delete',[PostController::class,'delete_post'])->name('admin.posts.delete');





	
		Route::get('/admin/users',[UserController::class,'index'])->name('admin.users');

		Route::get('/admin/user/create',[UserController::class,'create'])->name('admin.users.create');

		Route::post('/admin/user/create',[UserController::class,'store'])->name('admin.users.store');

		Route::get('/admin/user/{id}',[UserController::class,'edit'])->name('admin.users.edit');

		Route::post('/admin/user/{id}',[UserController::class,'update'])->name('admin.users.update');

		Route::post('/admin/user/{id}/delete',[UserController::class,'delete_user'])->name('admin.users.delete');
});







