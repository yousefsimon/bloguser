<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider and all of them will
  | be assigned to the "api" middleware group. Make something great!
  |
 */

/* Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
  return $request->user();
  });
 */



Route::post('/register', [\App\Http\Controllers\API\AuthController::class, 'register']);
Route::post('/login', [\App\Http\Controllers\API\AuthController::class, 'login']); 

Route::post('/verify_number', [\App\Http\Controllers\API\ValidateNumberController::class, 'verifyNumber']);

Route::post('/verify_code', [\App\Http\Controllers\API\ValidateNumberController::class, 'verifyCode']) ;       
        
Route::group(['middleware' => ['auth:api']], function () {
     Route::get('/profile', [\App\Http\Controllers\API\AuthController::class, 'profile']);
	 
	 Route::get('/users', [\App\Http\Controllers\API\PostController::class, 'users']);
     Route::get('/posts', [\App\Http\Controllers\API\PostController::class, 'posts']);       
     Route::post('/create_post', [\App\Http\Controllers\API\PostController::class, 'create_post']);      
        
});


